// finalloprojecto.cpp : Defines the entry point for the console application.
//

#include <tchar.h>
#include "stdafx.h"
#include "constants.h"

#ifdef LANGUAGE eng
#include "textEng.h"
#endif

#ifdef LANGUAGE fin
#include "textFin.h"
#endif


/**
* return 0
* @param None
* @author MaTuKa
* @date 30.11.2016
*/
int readRegistry() {

	HKEY key; /** Read registry value 1 */
	DWORD dwDisposition; /** Read registry key value 2 */

	std::cout << readingValues << std::endl;

	DWORD cbVal = initialAndReturn0; /** Read registry key value 3 */
	DWORD cbData; /** Read registry key value 4 */

	char value[thousand24]; /** Read registry key value 5 */
	DWORD string_len = thousand24; /** Read registry key value 6 */
	DWORD sType = REG_SZ; /** Read registry key value 7 */

	//! if statement if opening registry is success or not
	if (RegOpenKey(HKEY_CURRENT_USER, TEXT("Software\\TaMuKa\\"), &key) != ERROR_SUCCESS) {
		std::cout << oopsSomethingWrong << std::endl;
		_getch();
		return initialAndReturn0;
	}
	else {
		RegQueryValueExW(
			key,
			L"Integeri",
			NULL,
			NULL,
			(LPBYTE)&cbVal,
			&cbData
		);

		RegQueryValueExW(
			key,
			L"Stringeri",
			NULL,
			&sType,
			(LPBYTE)&value,
			&string_len
		);

		std::cout << valueees << std::endl;
		std::cout << "Integer: " << cbVal << std::endl;
		std::cout << "String: " << value << std::endl;

	}

	std::cout << doSomething << std::endl;
	_getch();
	return initialAndReturn0;

}


/**
* Main
* @author TaMuKa
* @date 30.11.2016
*/
int main(int argc, char* argv[])
{


	HKEY key; /** main value 1 */
	DWORD dwDisposition; /** main value 2 */

	//! Create new key to path hkey_current_user\software\TaMuKa
	RegCreateKeyExW(
		HKEY_CURRENT_USER,
		L"Software\\TaMuKa",
		initialAndReturn0,
		NULL,
		REG_OPTION_NON_VOLATILE,
		KEY_ALL_ACCESS,
		NULL,
		&key,
		&dwDisposition
	);

	//! if statement if there is only one argument
	if (argc == onlyOneArgument) {

		//! Convert argv1 to string
		std::string onlyParameter = argv[numberOne];

		//! if statement checking value of first argument
		if (onlyParameter == "-h" || onlyParameter == "-help") {
			printf(helpText);
			printf(writeText);
			printf(readText);
			printf(noArgsText);
		}
		else if (onlyParameter == "-r" || onlyParameter == "-read") {

			readRegistry();

		}
		else if (onlyParameter == "-w" || onlyParameter == "-write") {

			//! Ask user if he wants to write integer or string
			int userInput = initialAndReturn0;
			std::cout << giveIntOrString << std::endl;
			std::cout << "******" << std::endl;
			std::cout << "1 - Integer" << std::endl;
			std::cout << "2 - String" << std::endl;
			std::cin >> userInput;

			//! if userinput were 1
			if (userInput == numberOne) {

				std::cin.ignore();
				int userInt = initialAndReturn0;
				std::cout << intText << std::endl;
				std::cin >> userInt;

				DWORD userIntegeri = userInt;

				RegSetValueExW(
					key,
					L"Integeri",
					initialAndReturn0,
					REG_DWORD,
					reinterpret_cast<BYTE *>(&userIntegeri),
					sizeof(userIntegeri)
				);
			} 
			else if (userInput == onlyOneArgument) { //! if userinput were 2

				std::cin.ignore();
				char userStr[thousand24];
				std::cout << stringText << std::endl;
				std::cin.getline(userStr, thousand24);

				char* userStringeri = userStr;

				RegSetValueExW(
					key,
					L"Stringeri",
					initialAndReturn0,
					REG_SZ,
					(LPBYTE)userStringeri,
					strlen(userStringeri) + numberOne
				);
			}
			else { //! if userinput were something else show message and close application
				printf(oopsSomethingWrong);
				_getch();
				return initialAndReturn0;
			}


			printf(valueWriten);
			_getch();
			return initialAndReturn0;

		}
		else {
			printf(unknownArg);
			_getch();
			return initialAndReturn0;
		}
	}
	else if (argc == 1) { //! If no arguments given will create, read and write registry keys and values

		//! Ask user if he wants to write integer or string
		int userInput = initialAndReturn0;
		std::cout << giveIntOrString << std::endl;
		std::cout << "******" << std::endl;
		std::cout << "1 - Integer" << std::endl;
		std::cout << "2 - String" << std::endl;
		std::cin >> userInput;

		if (userInput == numberOne) {

			std::cin.ignore();
			int userInt = initialAndReturn0;
			std::cout << intText << std::endl;
			std::cin >> userInt;

			DWORD userIntegeri = userInt;

			RegSetValueExW(
				key,
				L"Integeri",
				initialAndReturn0,
				REG_DWORD,
				reinterpret_cast<BYTE *>(&userIntegeri),
				sizeof(userIntegeri)
			);
		}
		else if (userInput == onlyOneArgument) {

			std::cin.ignore();
			char userStr[thousand24];
			std::cout << stringText << std::endl;
			std::cin.getline(userStr, thousand24);

			char* userStringeri = userStr;

			RegSetValueExW(
				key,
				L"Stringeri",
				initialAndReturn0,
				REG_SZ,
				(LPBYTE)userStringeri,
				strlen(userStringeri) + numberOne
			);
		}
		else {
			printf(oopsSomethingWrong);
			_getch();
			return initialAndReturn0;
		}
	}

	std::cout << readingValues << std::endl;

	DWORD cbVal = initialAndReturn0; /** main value 3 */
	DWORD cbData; /** main value 4 */

	char value[thousand24]; /** main value 5 */
	DWORD string_len = thousand24; /** main value 6 */
	DWORD sType = REG_SZ; /** main value 7 */

	 //! if statement checking if opening registry is success or not
	if (RegOpenKey(HKEY_CURRENT_USER, TEXT("Software\\TaMuKa\\"), &key) != ERROR_SUCCESS) {
		std::cout << oopsSomethingWrong << std::endl;
		_getch();
		return initialAndReturn0;
	}
	else {
		RegQueryValueExW(
			key,
			L"Integeri",
			NULL,
			NULL,
			(LPBYTE)&cbVal,
			&cbData
		);

		RegQueryValueExW(
			key,
			L"Stringeri",
			NULL,
			&sType,
			(LPBYTE)&value,
			&string_len
		);

		std::cout << valueees << std::endl;
		std::cout << "Integer: " << cbVal << std::endl;
		std::cout << "String: " << value << std::endl;

	}


	printf(doSomething);
	_getch();

    return initialAndReturn0;
}

