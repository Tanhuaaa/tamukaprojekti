#pragma once

#define helpText "'-h' or '-help' will print all available command line options\n"
#define writeText "'-w' or '-write' will write new value to registry\n"
#define readText "'-r' or '-read' will print registry values\n"
#define noArgsText "If no given arguments program will ask if you wanna write integer or string value to registry\n"

#define readingValues "Reading values from registry..."
#define oopsSomethingWrong "Oops.. something went wrong\n"
#define unknownArg "Oops... Unknown argument! To get help run application again with -help argument\n"
#define giveIntOrString "Do you wanna write integer or string value to registry?"
#define intText "Give integer you wanna register to registry"
#define stringText "Give string you wanna register to registry"
#define valueWriten "Value writen to registry.. Press anything to continue\n"
#define doSomething "Press any key to continue..."
#define valueees "***VALUES***"