#pragma once

#define helpText "'-h' tai '-help' tulostaa kaikki kaytettävissa olevat komentorivi vaihtoehdot\n"
#define writeText "'-w' tai '-write' kirjoita uusi arvo rekisteriin\n"
#define readText "'-r' tai '-read' tulosta rekisterin arvot\n"
#define noArgsText "Jos argumentteja ei ole, sovellus kysyy haluatko kirjoittaa rekisteriin inter tai string arvon\n"

#define readingValues "Luetaan arvoja rekisterista..."
#define oopsSomethingWrong "Hups.. Jotain meni pieleen\n"
#define unknownArg "Oops... Tuntematon argumentti! Apua saat kaynnistamalla sovelluksen uudelleen -help argumentin kanssa\n"
#define giveIntOrString "Haluatko kirjoittaa integerin vai stringin rekisteriin?"
#define intText "Anna integer arvo, jonka haluat kirjoittaa rekisteriin"
#define stringText "Anna string, jonka haluat kirjoittaa rekisteriin"
#define valueWriten "Arvo kirjoitettu rekisteriin... Paina mita vain jatkaaksesi\n"
#define doSomething "Paina mita vain jatkaaksesi..."
#define valueees "***ARVOT***"